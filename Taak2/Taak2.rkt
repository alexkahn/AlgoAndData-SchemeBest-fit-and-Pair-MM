#lang r6rs

;-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
;-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
;-*-*                                                                 *-*-
;-*-*           Best-fit Memory Manager (Storage Management)          *-*-
;-*-*                                                                 *-*-
;-*-*        Theo D'Hondt - Wolfgang De Meuter - Alexandre Kahn       *-*-
;-*-*              1993-2009 Programming Technology Lab               *-*-
;-*-*                   Vrije Universiteit Brussel                    *-*-
;-*-*                                                                 *-*-
;-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
;-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-

(library
 (best-fit-mm)
 (export null make-vector vector? vector-length vector-ref vector-set! vector-free
         peek poke! make-vctr memory memory-size reset-trees smallest-size insert-free untag cons) ; Nieuwe functies aan export toegevoegd
 (import
  (rnrs control)
  (rnrs io simple (6))
  (rename (except (rnrs base) vector? vector-length)
          (set! scheme:set!) (vector-set! scheme:vector-set!) (vector-ref scheme:vector-ref) (make-vector scheme:make-vector)
          (cons scheme:cons) (pair? scheme:pair?) (car scheme:car) (cdr scheme:cdr))) ;aangepast nu we een eigen implementatie van car en cdr gebruiken

 (define null ())
 (define smallest-size 5)

 (define pair-tag 'pair) ; toegevoegd voor de nieuwe functionaliteiten
 (define (tag-pair addr) ; toegevoegd voor de nieuwe functionaliteiten
   (scheme:cons pair-tag addr)) ; toegevoegd voor de nieuwe functionaliteiten
 (define pair-size 2) ; toegevoegd voor de nieuwe functionaliteiten

 (define memory-size 100)
 (define memory (scheme:make-vector memory-size null))

 (define (peek addr)
   (scheme:vector-ref memory addr))

 (define (poke! addr value)
   (scheme:vector-set! memory addr value))

 (define address-tree null)
 (define size-tree    null)
 (define pair-tree    null)    ; sorteert de vrije pairs op adres
 (define rightmost-vector 0)  ; moet bij het alloceren van een vector gelijk zijn aan address nieuwe vector + size
 (define leftmost-pair (- memory-size 1)); leftmost-pair is bij het initialiseren nog niet aangemaakt en zal dus het grootste adres zijn dat er bestaat. Moet gelijk zijn aan het meest linkse adres van een pair
 (define rightmost-old-size memory-size) ; toegevoegd voor de nieuwe functionaliteiten

 (define (size addr)
   (peek (+ addr 0)))

 (define (size! addr siz)
   (poke! (+ addr 0) siz))

 (define (size-left addr)
   (peek (+ addr 1)))

 (define (size-left! addr size-left)
   (poke! (+ addr 1) size-left))

 (define (size-right addr)
   (peek (+ addr 2)))

 (define (size-right! addr size-right)
   (poke! (+ addr 2) size-right))

 (define (address-left addr)
   (peek (+ addr 3)))

 (define (address-left! addr addr-left)
   (poke! (+ addr 3) addr-left))

 (define (address-right addr)
   (peek (+ addr 4)))

 (define (address-right! addr addr-right)
   (poke! (+ addr 4) addr-right))

 ; in de pair-tree zal de car (n) naar de left pair-tree wijzen en de cdr (n-1) wijzen naar de right pair-tree, hieronder staan de functionaliteiten om door de boom te gaan

 (define (pair-left addr) ; toegevoegd voor de nieuwe functionaliteiten
   (peek (+ addr 0)))

 (define (pair-left! addr addr-left) ; toegevoegd voor de nieuwe functionaliteiten
   (poke! (+ addr 0) addr-left))

 (define (pair-right addr) ; toegevoegd voor de nieuwe functionaliteiten
   (peek (- addr 1)))

 (define (pair-right! addr addr-right) ; toegevoegd voor de nieuwe functionaliteiten
   (poke! (- addr 1) addr-right))

 (define (cdr-address addr) ; toegevoegd voor de nieuwe functionaliteiten
   (- addr 1))

 (define (find-address addr)
   (let lookup
     ((curr address-tree))
     (cond
       ((null? curr)
        ())
       ((= curr addr)
        curr)
       ((< addr curr)
        (lookup (address-left curr)))
       (else
        (let ((try (lookup (address-right curr))))
          (if (null? try)
              curr
              try))))))

 (define (find-size req-size)
   (let lookup
     ((curr size-tree))
     (cond
       ((null? curr)
        ())
       ((= (size curr) req-size)
        curr)
       ((< (size curr) req-size)
        (lookup (size-right curr)))
       (else
        (let ((try (lookup (size-left curr))))
          (if (null? try)
              curr
              try))))))

 (define (insert-free addr siz)
   (define (insert-address curr)
     (if (< curr addr)
         (let
             ((right (address-right curr)))
           (if (null? right)
               (address-right! curr addr)
               (insert-address right)))
         (let
             ((left (address-left curr)))
           (if (null? left)
               (address-left! curr addr)
               (insert-address left)))))
   (define (insert-size curr)
     (if (< siz (size curr))
         (let
             ((left (size-left curr)))
           (if (null? left)
               (size-left! curr addr)
               (insert-size left)))
         (let
             ((right (size-right curr)))
           (if (null? right)
               (size-right! curr addr)
               (insert-size right)))))
   (size! addr siz)
   (address-left!  addr null)
   (address-right! addr null)
   (size-left!     addr null)
   (size-right!    addr null)
   (when (< addr leftmost-pair) ; iets dat vrijkomt voorbij de leftmost-pair moet niet in de bomen geplaatste worden
     (if (null? address-tree)
         (scheme:set! address-tree addr)
         (insert-address address-tree))
     (if (null? size-tree)
         (scheme:set! size-tree addr)
         (insert-size size-tree))))

 (define (insert-pair addr) ;functie om de lege pairs in de pair-tree te plaatsen
   (define (pair-tree-loop curr)
     (when (> curr rightmost-vector)
       (if (< curr addr)
           (let
               ((right (pair-right curr)))
             (if (null? right)
                 (pair-right! curr addr)
                 (pair-tree-loop right)))
           (let
               ((left (pair-left curr))
                (mem memory))
             (if (null? left)
                 (pair-left! curr addr)
                 (pair-tree-loop left))))))
   (poke! addr null)                                           ;; Zet de kinderen van de leaf op null
   (poke! (- addr 1) null)
   (when (> addr (+ rightmost-vector smallest-size))
     (if (null? pair-tree)
         (scheme:set! pair-tree addr)
         (pair-tree-loop pair-tree))))

 (define (delete-free addr)
   (define siz (size addr))
   (define (delete-next-address curr op)
     (cond
       ((null? (address-right curr))
        (op (address-left curr))
        curr)
       (else
        (delete-next-address (address-right curr)
                             (lambda (ref)
                               (address-right! curr ref))))))
   (define (delete-address curr op)
     (when (real? curr) ; aangepast
       (cond
         ((> addr curr)
          (delete-address (address-right curr)
                          (lambda (ref)
                            (address-right! curr ref))))
         ((< addr curr)
          (delete-address (address-left curr)
                          (lambda (ref)
                            (address-left! curr ref))))
         ((null? (address-left curr))
          (op (address-right curr)))
         ((null? (address-right curr))
          (op (address-left curr)))
         (else
          (let
              ((hold (delete-next-address (address-left curr)
                                          (lambda (ref)
                                            (address-left! curr ref)))))
            (address-left! hold (address-left curr))
            (address-right! hold (address-right curr))
            (op hold))))))
   (define (delete-next-size curr op)
     (cond
       ((null? (size-left curr))
        (op (size-right curr))
        curr)
       (else
        (delete-next-size (size-left curr)
                          (lambda (ref)
                            (size-left! curr ref))))))
   (define (delete-size curr op)
     (cond
       ((< siz (size curr))
        (delete-size (size-left curr)
                     (lambda (ref)
                       (size-left! curr ref))))
       ((> siz (size curr))
        (delete-size (size-right curr)
                     (lambda (ref)
                       (size-right! curr ref))))
       ((not (= addr curr))
        (delete-size (size-right curr)
                     (lambda (ref)
                       (size-right! curr ref))))
       ((null? (size-left curr))
        (op (size-right curr)))
       ((null? (size-right curr))
        (op (size-left curr)))
       (else
        (let
            ((hold (delete-next-size (size-right curr)
                                     (lambda (ref)
                                       (size-right! curr ref)))))
          (size-left! hold (size-left curr))
          (size-right! hold (size-right curr))
          (op hold)))))
   (delete-address address-tree (lambda (ref)
                                  (scheme:set! address-tree ref)))
   (delete-size size-tree (lambda (ref)
                            (scheme:set! size-tree ref))))

 (define (delete-pair addr) ;functie om een pair uit de pair-tree te halen
   (define (delete-next-address curr op)
     (cond
       ((eq? (pair-right curr) null)
        (op (pair-left curr))
        curr)
       (else
        (delete-next-address (pair-right curr)
                             (lambda (ref)
                               (pair-right! curr ref))))))
   (define (delete-address curr op) ; moet ook uit de address-tree gehaald worden
     (when (real? curr)
       (cond
         ((> addr curr)
          (delete-address (pair-right curr)
                          (lambda (ref)
                            (pair-right! curr ref))))
         ((< addr curr)
          (delete-address (pair-left curr)
                          (lambda (ref)
                            (pair-left! curr ref))))
         ((eq? (pair-left curr) null)
          (op (pair-right curr)))
         ((eq? (pair-right curr) null)
          (op (pair-left curr)))
         (else
          (let
              ((hold (delete-next-address (pair-left curr)
                                          (lambda (ref)
                                            (pair-left! curr ref)))))
            (pair-left! hold (pair-left curr))
            (pair-right! hold (pair-right curr))
            (op hold))))))
   (delete-address pair-tree (lambda (ref)
                               (scheme:set! pair-tree ref))))

 (define (fix-size-tree) ; functie om de size-tree niet constant te moeten aanpassen
   (let
       ((new-siz (scheme:vector-ref memory rightmost-vector)))
     (when (and (not (= new-siz rightmost-old-size)) (not (null? size-tree)))
       (delete-free rightmost-vector)
       (insert-free rightmost-vector new-siz)
       (scheme:set! rightmost-old-size new-siz))))

 (define (reset-trees)
   (scheme:set! address-tree null)
   (scheme:set! size-tree   null)
   (scheme:set! pair-tree null)) ; ook de pair-tree moet in dit geval gereset worden

 (define (initialize)
   (define (pair-loop curr) ; In deze loop wordt de pair-tree opgevuld
     (when (> curr (+ rightmost-vector 1))
       (insert-pair curr)
       (pair-loop (- curr 2))))
   (pair-loop (- memory-size 1))
   (insert-free 0 memory-size))

 (define vector-tag 'vector)

 (define (make-vctr addr)
   (scheme:cons vector-tag addr)) ;aangepast "scheme:"

 (define (untag vctr) ; Werkt zowel voor vectoren als pairs
   (scheme:cdr vctr)) ;aangepast "scheme:"

 (define (vector? any)
   (and (scheme:pair? any) ;aangepast "scheme:"
        (eq? (scheme:car any) vector-tag)))

 (define (make-vector size)
   (scheme:set! size (+ size 1))
   (fix-size-tree) ; hier wordt de size-tree aangepast indien nodig
   (if (< size smallest-size)
       (scheme:set! size smallest-size))
   (let
       ((found-addr (find-size size)))
     (if (null? found-addr)
         null ;(error "Memory overflow" make-vector)
         (let
             ((found-size (peek found-addr)))
           (delete-free found-addr)
           (delete-range found-addr size) ; dient om de respectievelijke pairs uit de pair-tree te halen
           (cond
             ((< smallest-size (- found-size size))
              (insert-free (+ found-addr size) (- found-size size))
              (poke! found-addr size))
             (else
              (poke! found-addr found-size)))
         (when (> (+ found-addr size) rightmost-vector)
           (scheme:set! rightmost-vector (+ size found-addr))) ; aangepast
         (make-vctr found-addr)
         ))))
 ;;; volgende functies dienen om ranges toe te voegen en verwijderen ;;;
 (define (delete-range address size) ; Dient om een range addressen uit de boom te verwijderen
   (define (delete-loop address counter)
     (when (> size counter)
       (delete-pair (+ address 1))
       (delete-loop  (+ address pair-size) (+ counter pair-size))))
   (when (odd? size)
     (scheme:set! size (+ size 1)))
   (when (or (and (odd? memory-size) (and (odd? address)))(and (even? memory-size) (odd? address)))
     (scheme:set! address (+ address 1)))
   (delete-loop address 0))

 (define (add-range address size) ; Dient om een range addressen aan de boom toe te voegen
   (define (add-loop curr)
     (when (and (< curr leftmost-pair) (> curr rightmost-vector))
       (insert-pair curr)
       (add-loop (- curr 2))))
   (when (or (and (odd? memory-size) (and (odd? address))) (and (even? memory-size) (even? address)))
     (scheme:set! address (- address 1)))
   (add-loop address))
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 (define (cons car cdr) ; nieuwe cons functie voor geheugen cellen
   (define (biggest-address previous tree)
     (cond ((eq? tree null) previous)
           (else
            (biggest-address tree (pair-right tree)))))
   (define addr (biggest-address null pair-tree))
   (when (eq? null addr)
     (error "memory overflow" cons))
   (when (< addr leftmost-pair)
     (scheme:set! leftmost-pair addr))
   (delete-pair addr)
   (let
       ((found-size (peek rightmost-vector)))
     (cond
       ((< smallest-size (- found-size pair-size))
        (scheme:vector-set! memory rightmost-vector (- (scheme:vector-ref memory rightmost-vector) pair-size))
        )))
   (scheme:vector-set! memory addr car)
   (scheme:vector-set! memory (cdr-address addr) cdr)
   (tag-pair addr))

 (define (vector-free vctr)
   (define addr (untag vctr))
   (define siz (peek addr))
   (define size siz)
   (fix-size-tree)
   (let
       ((right-addr (find-address (+ addr siz))))
     (if (not (null? right-addr))
         (let
             ((right-size (peek right-addr)))
           (when (= right-addr (+ addr siz))
             (delete-free right-addr)
             (scheme:set! siz (+ siz right-size))))))
   (let
       ((left-addr (find-address addr)))
     (if (not (null? left-addr))
         (let
             ((left-size (peek left-addr)))
           (scheme:set! size (+ size left-size))
           (when (= (+ left-addr left-size) addr)
             (delete-free left-addr)
             (scheme:set! addr left-addr)
             (scheme:set! siz (+ siz left-size))))))
   (when (> (+ addr siz) rightmost-vector)
     (scheme:set! rightmost-vector addr)
     (add-range (+ addr size) size)) ; Hier worden de vrijgemaakte paren terug aan de boom toe gevoegd
   (display "size ")(display siz)(newline)
   (insert-free addr siz))

 (define (pair-free pair) ; functie om cons-cellen vrij te maken
   (define addr (untag pair))
   (insert-pair addr)
   (when (= addr leftmost-pair)
     (scheme:set! leftmost-pair (second-pair-from-left))
     ;als een pair terug gezet wordt moet de size aangepast worden, maar niet de address tree
     (scheme:vector-set! memory rightmost-vector (+ 2 (scheme:vector-ref memory rightmost-vector)))))

 (define (second-pair-from-left) ; dient om de eerstvolgende gebruikte pair te zoeken, nuttig bij het vrijmaken van de leftmost-pair
   (define (in-tree? addr tree)
     (cond
       ((eq? addr tree) #t)
       ((null? tree) #f)
       ((> addr tree) (in-tree? addr (pair-left tree)))
       ((< addr tree) (in-tree? addr (pair-right tree)))))
   (define (loop address) ;in freepair zoeken of die er in zit
     (cond
       ((>= address memory-size) null)
       ((in-tree? address pair-tree) (loop (+ address 2)))
       (else address)))
   (loop (+ 2 leftmost-pair))) ; we doen + 2 aangezien de huidige leftmost zeker niet de nieuwe zal worden

 (define (address+index addr index)
   (let
       ((size (- (peek addr) 1)))
     (if (or (< size 0)
             (>= index size))
         (error "index out of bounds" index)
         (+ addr index 1))))

 (define (vector-ref vctr index)
   (define addr (untag vctr))
   (peek (address+index addr index)))

 (define (vector-set! vctr index value)
   (define addr (untag vctr))
   (poke! (address+index addr index) value))

 (define (vector-length vctr)
   (define addr (untag vctr))
   (- (peek addr) 1))
 (define (vector-init vec sym)
   (do ((i 0 (+ 1 i)))
     ((= i (vector-length vec)))
     (vector-set! vec i sym)))
 (initialize)

 ; Test cases

 ;(display memory) (newline)
 ;;(define a (make-vector 10))
 ;;(define b (make-vector 9))
 ;;(define c (make-vector 8))
 ;;(define d (make-vector 7))
 ;(define e (make-vector 10))
 ;(display memory) (newline)
 ;;(define cons-1 (cons 1 2))
 ;(display memory) (newline)
 ;;(vector-free b)
 ;;(vector-free c)
 ;;(vector-free d)
 ;
 ;;(vector-free e)
 ;(display memory)(newline)
 ;;(define cons-2 (cons 45 30))
 ;;(define cons-3 (cons 'a 'b))
 ;(define to-big (make-vector 10))
 ;;(define cons-3 (cons 'aa 'bb))
 ;(display "1e cons")(newline)
 ;(display memory)(newline)
 ;;(pair-free cons-2)
 ;(display memory)(newline)
 ;;(vector-free a)
 ;;(pair-free cons-3)
 ;(display memory)

 )
